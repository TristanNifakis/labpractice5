public class School{
	public static void main(String args[]){
		Student s = new Student(12222,"Dan","CS");
		System.out.println(s);
		
		Student[] pupil = new Student[2];
		pupil[0] = new Student(2032788,"Tristan","CS");
		pupil[1] = new Student(10483228,"Mason","Blackops");
		
		Classroom class1 = new Classroom("java",420,pupil);
		System.out.println(class1);
	}
}
