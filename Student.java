public class Student {
	private int studentId;
	private String name;
	private String program;
	
	public Student(int studentId,String name,String program) {
		this.studentId = studentId;
		this.name = name;
		this.program = program;
	}
	
	public String toString(){
		return this.name+": "+this.studentId+", "+this.program+" student; ";
	}
}
