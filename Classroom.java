public class Classroom{
	//fields
	private String subject = "Computer science";
	private int code = 420;
	private Student[] students;
	//costructor
	public Classroom(String subject,int code, Student[] students){
		this.subject = subject;
		this.code = code;
		this.students = students;
	}
	//getters
	public String getSubject(){
		return this.subject;
	}
	public int getCode(){
		return this.code;
	}
	public Student[] getStudent(){
		return this.students;
	}
	
	
	public String toString(){
		String s = "";
		for(int i=0 ;i<students.length;i++){
			s = s+this.students[i];
		}
		return "You take "+this.subject+ " " +this.code+" and it contains "+s;
	}
}
	